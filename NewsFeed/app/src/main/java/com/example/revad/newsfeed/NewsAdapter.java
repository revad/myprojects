package com.example.revad.newsfeed;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

/**
 * Created by revad on 3/16/2018.
 */

public class NewsAdapter extends ArrayAdapter<NewsData> {

    public NewsAdapter(@NonNull Context context, List<NewsData> news) {
        super(context, 0, news);
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        NewsData currentNews = getItem (position);

        TextView titleView = (TextView) listItemView.findViewById(R.id.titleView);
        TextView sectionView = (TextView) listItemView.findViewById(R.id.sectionView);
        TextView dateView = (TextView) listItemView.findViewById(R.id.dateView);

        titleView.setText("Title: " + currentNews.getTitle());
        sectionView.setText("Section: " + currentNews.getSection());
        dateView.setText("Date: " + currentNews.getDate());

        ViewGroup.LayoutParams params = listItemView.getLayoutParams();
        params.height = 0;
        listItemView.setLayoutParams(params);

        return listItemView;
    }
}
