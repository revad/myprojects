package com.example.revad.newsfeed;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import static com.example.revad.newsfeed.MainActivity.LOG_TAG;

/**
 * Created by revad on 3/16/2018.
 */

public class FetchUtils {
    private FetchUtils() {
    }

    public static ArrayList<NewsData> fetchNewsData(String requestUrl) {

        URL url = createUrl(requestUrl);

        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }

        ArrayList<NewsData> news = extractNews(jsonResponse);

        return news;
    }

    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL ", e);
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving the news JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    public static ArrayList<NewsData> extractNews(String newsJSON) {

        ArrayList<NewsData> news = new ArrayList<>();

        try {

            JSONObject jObject = new JSONObject(newsJSON);
            JSONObject jResponse = jObject.getJSONObject("response");
            JSONArray jResults = jResponse.getJSONArray("results");
            for (int i = 0; i < jResults.length(); i++) {

                JSONObject properties = jResults.getJSONObject(i);

                String mTitle = properties.getString("webTitle");
                String mSection = properties.getString("sectionName");

                String mDate = properties.getString("webPublicationDate");
                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date d = inputFormat.parse(mDate);
                String formattedDate = outputFormat.format(d);

                String mUrl = properties.getString("webUrl");

                news.add(new NewsData(mTitle, mSection, formattedDate, mUrl));
            }

        } catch (JSONException e) {
            Log.e("FetchUtils", "Problem parsing the news JSON results", e);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return news;
    }
}
