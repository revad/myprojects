package com.example.revad.countrieshandbook;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import com.bumptech.glide.RequestBuilder;

public class SearchByNameAdapter extends RecyclerView.Adapter<SearchByNameAdapter.SearchByNameViewHolder> {

    private ArrayList<CountryDataShort> mCountriesData;
    private ArrayList<CountryDataShort> mCountriesDataUnfilteredCopy;
    final private SearchByNameAdapter.ListItemClickListener mOnClickListener;
    private Context context;
    private RequestBuilder<PictureDrawable> requestBuilder;
    private String searchMode;

    public interface ListItemClickListener {
        void onListItemClick(View v, CountryDataShort cData, int position);
    }

    public SearchByNameAdapter(SearchByNameAdapter.ListItemClickListener listener, ArrayList<CountryDataShort> countriesData) {
        mOnClickListener = listener;
        mCountriesData = new ArrayList<>();
        mCountriesData.addAll(countriesData);
        mCountriesDataUnfilteredCopy = new ArrayList<>();
        mCountriesDataUnfilteredCopy.addAll(countriesData);
    }

    @Override
    public SearchByNameAdapter.SearchByNameViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        context = viewGroup.getContext();
        int listItemLayoutId = R.layout.search_by_name_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(listItemLayoutId, viewGroup, false);
        SearchByNameAdapter.SearchByNameViewHolder viewHolder = new SearchByNameAdapter.SearchByNameViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchByNameAdapter.SearchByNameViewHolder holder, final int position) {

        if (searchMode.equals(MainActivity.SEARCH_MODE_BY_NAME) | searchMode.equals(MainActivity.SEARCH_MODE_ALL_COUNTRIES) |
                searchMode.equals(MainActivity.SEARCH_MODE_BY_REGION) | searchMode.equals(MainActivity.SEARCH_MODE_SHOW_HISTORY)) {
            holder.countryName.setText(mCountriesData.get(position).getName());
            holder.capitalCity.setText(mCountriesData.get(position).getCapitalCity());
        }

        if (searchMode.equals(MainActivity.SEARCH_MODE_BY_CAPITAL_CITY)) {
            holder.countryName.setText(mCountriesData.get(position).getCapitalCity());
            holder.capitalCity.setText(mCountriesData.get(position).getName());
        }

        if (searchMode.equals(MainActivity.SEARCH_MODE_BY_LANGUAGE)) {
            holder.countryName.setText(mCountriesData.get(position).getName());
            String lList = mCountriesData.get(position).getLanguages().toString().replace("[", "").replace("]", "");
            holder.capitalCity.setText(lList);
        }

        requestBuilder = GlideApp.with(context)
                .as(PictureDrawable.class)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.error_placeholder).listener(new com.example.revad.countrieshandbook.SvgSoftwareLayerSetter());

        Uri uri = Uri.parse(mCountriesData.get(position).getFlag());

        requestBuilder.load(uri).into(holder.flagImage);
        holder.flagImage.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        holder.itemView.setContentDescription(holder.countryName.getText().toString());
        holder.itemView.setFocusable(true);
    }

    public void filter(String filterQuery) {
        int i=0;
        mCountriesData.clear();
        if (searchMode.equals(MainActivity.SEARCH_MODE_BY_NAME)) {
            while (i < mCountriesDataUnfilteredCopy.size()) {
                if (mCountriesDataUnfilteredCopy.get(i).getName().toLowerCase().contains(filterQuery.toLowerCase())) {
                    mCountriesData.add(mCountriesDataUnfilteredCopy.get(i));
                }
                i++;
            }
        }

        if (searchMode.equals(MainActivity.SEARCH_MODE_BY_CAPITAL_CITY)) {
            while (i < mCountriesDataUnfilteredCopy.size()) {
                if (mCountriesDataUnfilteredCopy.get(i).getCapitalCity().toLowerCase().contains(filterQuery.toLowerCase())) {
                    mCountriesData.add(mCountriesDataUnfilteredCopy.get(i));
                }
                i++;
            }
        }
        notifyDataSetChanged();
    }

    public void setSearchMode(String sMode) {
        searchMode = sMode;
    }

    @Override
    public int getItemCount() {
        return mCountriesData.size();
    }

    public CountryDataShort getCountryData(int item) {
        return mCountriesData.get(item);
    }

    public void clear() {
        mCountriesData.clear();
        update();
    }

    public void addAll(ArrayList<CountryDataShort> items) {
        mCountriesData.addAll(items);
        update();
    }

    public ArrayList<CountryDataShort> getCountriesData () {
        return mCountriesData;
    }

    private void update() {
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        mCountriesData.remove(position);
        notifyItemRemoved(position);
    }

    class SearchByNameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView flagImage;
        private TextView countryName;
        private TextView capitalCity;

        public SearchByNameViewHolder(View itemView) {
            super(itemView);
            flagImage = itemView.findViewById(R.id.flag_image_short);
            countryName = itemView.findViewById(R.id.country_name_short);
            capitalCity = itemView.findViewById(R.id.capital_city_short);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(flagImage, getCountryData(clickedPosition), clickedPosition);
        }
    }
}


