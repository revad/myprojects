package com.example.revad.popularmovies;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<Movie>>, MoviesAdapter.ListItemClickListener {

    private static final String MOVIE_DB_API_KEY = BuildConfig.API_KEY;
    private static final String MOVIES_REQUEST_BASE_URL = "https://api.themoviedb.org/3/movie/";
    private static final String MOVIES_REQUEST_URL_BY_POPULARITY = MOVIES_REQUEST_BASE_URL + "popular?api_key=" + MOVIE_DB_API_KEY;
    private static final String MOVIES_REQUEST_URL_BY_RATING = MOVIES_REQUEST_BASE_URL + "top_rated?api_key=" + MOVIE_DB_API_KEY;
    private static final int LOADER_ID_GET_MOVIES_LIST = 761293;
    private static final int LOADER_ID_GET_MOVIE_ADDITIONAL_DATA = 761294;
    private static final int LOADER_ID_GET_FAVORITE_MOVIES_LIST = 761295;
    private static final String MOVIES_URL_EXTRA = "movies_url";
    private static final String MOVIE_ID_EXTRA = "movie_id";
    private Uri movieQueryUri = MovieContract.MovieEntry.CONTENT_URI;
    private MoviesAdapter mAdapter;
    @BindView(R.id.spinner_widget) ProgressBar spinner;
    @BindView(R.id.rv_movies) RecyclerView mMoviesList;
    @BindView(R.id.empty_state_view) TextView emptyStateView;
    private int selectedMovieListId;
    private boolean showingFavorites = false;
    private boolean isConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        mMoviesList.setLayoutManager(layoutManager);
        mMoviesList.setHasFixedSize(true);

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if (isConnected){
            Bundle args = new Bundle();
            args.putString(MOVIES_URL_EXTRA, MOVIES_REQUEST_URL_BY_POPULARITY);
            getLoaderManager().initLoader(LOADER_ID_GET_MOVIES_LIST, args, this);
        }
        else
        {
            spinner.setVisibility(View.GONE);
            emptyStateView.setText(R.string.no_network);
            emptyStateView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public Loader<ArrayList<Movie>> onCreateLoader(int id, Bundle args) {
        spinner.setVisibility(View.VISIBLE);
        if (id == LOADER_ID_GET_MOVIE_ADDITIONAL_DATA) {
            return new MoviesListLoader(this, MOVIE_DB_API_KEY, args.getString(MOVIE_ID_EXTRA));
        }
        if (id == LOADER_ID_GET_FAVORITE_MOVIES_LIST) {
            if (mAdapter != null) mAdapter.clear();
            return new MoviesListLoader(this, movieQueryUri);
        }
        if (mAdapter != null) mAdapter.clear();
        return new MoviesListLoader(this, args.getString(MOVIES_URL_EXTRA));
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Movie>> loader, ArrayList<Movie> movies) {
        spinner.setVisibility(View.GONE);

        if (loader.getId() == LOADER_ID_GET_MOVIE_ADDITIONAL_DATA) {
            if (movies.size() == 0) { return; }
            Intent movieDetailIntent = new Intent(MainActivity.this, DetailsActivity.class);
            mAdapter.getMovieData(selectedMovieListId).setTrailers(movies.get(0).getTrailers());
            mAdapter.getMovieData(selectedMovieListId).setReviews(movies.get(0).getReviews());
            movieDetailIntent.putExtra("movie", mAdapter.getMovieData(selectedMovieListId));
            startActivity(movieDetailIntent);
            getLoaderManager().destroyLoader(LOADER_ID_GET_MOVIE_ADDITIONAL_DATA);
            getLoaderManager().destroyLoader(LOADER_ID_GET_FAVORITE_MOVIES_LIST);
            getLoaderManager().destroyLoader(LOADER_ID_GET_MOVIES_LIST);
        }
        else
        if (loader.getId() == LOADER_ID_GET_MOVIES_LIST || loader.getId() == LOADER_ID_GET_FAVORITE_MOVIES_LIST) {
            if (movies.size() == 0) {
                emptyStateView.setText(R.string.no_movies_found);
                emptyStateView.setVisibility(View.VISIBLE);
                mAdapter.clear();
                return;
            }
            mMoviesList.scrollToPosition(0);
            emptyStateView.setVisibility(View.INVISIBLE);
            if (mAdapter == null) {
                mAdapter = new MoviesAdapter(this, movies);
                mMoviesList.setAdapter(mAdapter);
            } else {
                mAdapter.clear();
                mAdapter.addAll(movies);
            }
            getLoaderManager().destroyLoader(LOADER_ID_GET_MOVIES_LIST);
        }
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Movie>> loader) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        if (!isConnected) {
            menu.findItem(R.id.action_rating).setVisible(false);
            menu.findItem(R.id.action_popular).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Bundle args = new Bundle();
        if (id == R.id.action_popular) {
            showingFavorites = false;
            args.putString(MOVIES_URL_EXTRA, MOVIES_REQUEST_URL_BY_POPULARITY);
            getLoaderManager().restartLoader(LOADER_ID_GET_MOVIES_LIST, args, this);
        }
        else
            if (id == R.id.action_rating) {
                showingFavorites = false;
                args.putString(MOVIES_URL_EXTRA, MOVIES_REQUEST_URL_BY_RATING);
                getLoaderManager().restartLoader(LOADER_ID_GET_MOVIES_LIST, args, this);
            }
        else
            if (id == R.id.action_favorites) {
                showingFavorites = true;
                getLoaderManager().restartLoader(LOADER_ID_GET_FAVORITE_MOVIES_LIST, args, this);
            }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick(Movie movieData, int position) {
        if (showingFavorites && !isConnected) {
            Intent movieDetailIntent = new Intent(MainActivity.this, DetailsActivity.class);
            movieDetailIntent.putExtra("favorite_movie_id", movieData.getId());
            startActivity(movieDetailIntent);
        } else {
            selectedMovieListId = position;
            Bundle args = new Bundle();
            args.putString(MOVIE_ID_EXTRA, movieData.getId());
            getLoaderManager().restartLoader(LOADER_ID_GET_MOVIE_ADDITIONAL_DATA, args, this);
        }
    }
}
