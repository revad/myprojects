package com.example.revad.bakingrecipes;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import java.util.ArrayList;

public class WidgetListFactory implements RemoteViewsService.RemoteViewsFactory {

    private Context mContext;
    private ArrayList<Ingredient> rIngredients;
    private Intent mIntent;

    public WidgetListFactory(Context context, Intent intent) {
        mContext = context;
        mIntent = intent;
    }

    @Override
    public void onCreate() {
        Bundle bundle = mIntent.getBundleExtra("bundle");
        rIngredients = bundle.getParcelableArrayList("ingredients_object");
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews wList = new RemoteViews(mContext.getPackageName(), R.layout.widget_ingred_list_item);
        wList.setTextViewText(R.id.w_ingredient_name_view, rIngredients.get(position).getIngredient());
        wList.setTextViewText(R.id.w_ingredient_amount_view, rIngredients.get(position).getQuantity());
        wList.setTextViewText(R.id.w_ingredient_unit_view, rIngredients.get(position).getMeasure().toLowerCase());
        return wList;
    }

    @Override
    public int getCount(){
        if (rIngredients != null) { return rIngredients.size(); }
            else return 1;
    }

    @Override
    public void onDataSetChanged(){ }

    @Override
    public int getViewTypeCount() { return 1; }

    @Override
    public long getItemId(int i) { return i; }

    @Override
    public void onDestroy(){ }

    @Override
    public boolean hasStableIds() { return true; }

    @Override
    public RemoteViews getLoadingView() { return null; }
}