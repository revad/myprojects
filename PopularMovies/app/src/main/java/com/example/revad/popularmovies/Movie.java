package com.example.revad.popularmovies;

public class Movie {

    private String mTitle;
    private String mPoster;
    private String mSummary;
    private String mRating;
    private String mReleaseDate;

    public Movie (String title, String poster, String summary, String rating, String releaseDate) {
        mTitle = title;
        mPoster = poster;
        mSummary = summary;
        mRating = rating;
        mReleaseDate = releaseDate;
    }

    public String getTitle () {
        return mTitle;
    }

    public String getPoster () {
        return mPoster;
    }

    public String getSummary () {
        return mSummary;
    }

    public String getRating () {
        return mRating;
    }

    public String getReleaseDate () {
        return mReleaseDate;
    }
}
