package com.example.revad.countrieshandbook;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.content.AsyncTaskLoader;
import android.os.Bundle;
import android.text.TextUtils;
import com.example.revad.countrieshandbook.db.CountryContract;
import java.util.ArrayList;

public class DataLoader extends AsyncTaskLoader<ArrayList<CountryDataShort>> {

    private Uri mQueryUri;
    private String[] languageData;
    private String mRegion;
    private String sortOrder = CountryContract.CountryEntry.COLUMN_COUNTRY_FULL_NAME + " ASC";

    private  static final String[] MAIN_PROJECTION = {
            CountryContract.CountryEntry.COLUMN_COUNTRY_FULL_NAME,
            CountryContract.CountryEntry.COLUMN_COUNTRY_CAPITAL_CITY,
            CountryContract.CountryEntry.COLUMN_COUNTRY_FLAG,
            CountryContract.CountryEntry.COLUMN_COUNTRY_DATA,
    };

    public DataLoader(Context context) {
        super(context);
    }

    public DataLoader(Context context, Bundle args) {
        super(context);
        if (args.containsKey(MainActivity.SEARCH_MODE_BY_LANGUAGE)) languageData = args.getStringArray(MainActivity.SEARCH_MODE_BY_LANGUAGE);
            else mRegion = args.getString(MainActivity.SEARCH_MODE_BY_REGION);
    }

    public DataLoader(Context context, Uri queryUri) {
        super(context);
        mQueryUri = queryUri;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public ArrayList<CountryDataShort> loadInBackground() {
        if (mQueryUri != null) {
            Cursor cursor = getContext().getContentResolver().query(mQueryUri, MAIN_PROJECTION, null,
                    null, sortOrder, null);
            ArrayList<CountryDataShort> countries = new ArrayList<>();
            while(cursor.moveToNext()) {
                countries.add( new CountryDataShort(
                        cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[0])),
                        cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[1])),
                        cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[2])),
                        null,
                        cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[3]))
                ) );
            }
            return countries;
        }

        if (languageData == null && TextUtils.isEmpty(mRegion)) return FetchData.fetchCountriesData("");
        if (!TextUtils.isEmpty(mRegion)) return FetchData.fetchCountriesData(mRegion);
        return FetchData.fetchCountriesDataWithLanguages(languageData);
    }
}
