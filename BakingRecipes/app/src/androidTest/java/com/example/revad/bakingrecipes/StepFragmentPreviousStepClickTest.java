package com.example.revad.bakingrecipes;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withTagValue;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
public class StepFragmentPreviousStepClickTest {
    public static final String STEP_NAME= "Prepare wet ingredients.";

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void selectRecipe() {
        onView(ViewMatchers.withId(R.id.rv_recipes))
                .perform(RecyclerViewActions.actionOnItemAtPosition(2, click()));
    }

    @Before
    public void selectFirstStep() {
        onView(allOf(withTagValue(is((Object) "textView4")), isDisplayed())).perform(click());
    }

    @Before
    public void selectNextStep() {
        onView(withId(R.id.prev_step_button)).perform(click());
    }

    @Test
    public void clickFirstStepListItem_OpensFirstStep() {
        onView(withId(R.id.shortDescriptionView)).check(matches(withText(STEP_NAME)));
    }
}
