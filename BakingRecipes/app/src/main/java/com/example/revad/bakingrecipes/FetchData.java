package com.example.revad.bakingrecipes;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class FetchData {

    private FetchData() {
    }

    private static String LOG_TAG = FetchData.class.getName();
    private static final String JSON_ID = "id";
    private static final String JSON_NAME = "name";
    private static final String JSON_CODE_NOT_SPECIFIED = "Not specified";

    public static ArrayList<Recipe> fetchRecipes(String requestUrl) {
        URL url;
        String jsonResponse = null;
        ArrayList<Recipe> recipes = new ArrayList<>();
        url = createUrl(requestUrl);
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }
        if (jsonResponse != null) {
            recipes.addAll(extractRecipes(jsonResponse));
        }
        return recipes;
    }

    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL ", e);
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        if (url == null) {
            Log.e(LOG_TAG, "Empty URL");
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
                jsonResponse = null;
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    private static ArrayList<Recipe> extractRecipes(String sRecipes) {
        ArrayList<Recipe> recipes = new ArrayList<>();
        try {
            JSONArray jResults = new JSONArray(sRecipes);
            for (int i=0; i<jResults.length(); i++) {
                JSONObject recipeItem = jResults.getJSONObject(i);
                String recipeId = recipeItem.optString(JSON_ID, JSON_CODE_NOT_SPECIFIED);
                String recipeName = recipeItem.optString(JSON_NAME, JSON_CODE_NOT_SPECIFIED);

                JSONArray jIngredients = recipeItem.getJSONArray("ingredients");
                ArrayList<Ingredient> ingredientsList = new ArrayList<>();
                for (int j=0; j<jIngredients.length(); j++) {
                    JSONObject ingredientItem = jIngredients.getJSONObject(j);
                    String ingredientItemQuantity = ingredientItem.optString("quantity", JSON_CODE_NOT_SPECIFIED);
                    String ingredientItemMeasure = ingredientItem.optString("measure", JSON_CODE_NOT_SPECIFIED);
                    String ingredientItemName = ingredientItem.optString("ingredient", JSON_CODE_NOT_SPECIFIED);
                    ingredientsList.add(new Ingredient(ingredientItemQuantity, ingredientItemMeasure, ingredientItemName));
                }

                JSONArray jSteps = recipeItem.getJSONArray("steps");
                ArrayList<Step> stepsList = new ArrayList<>();
                for (int j=0; j<jSteps.length(); j++) {
                    JSONObject stepItem = jSteps.getJSONObject(j);
                    String stepItemId = stepItem.optString("id", JSON_CODE_NOT_SPECIFIED);
                    String stepItemShortDescription = stepItem.optString("shortDescription", JSON_CODE_NOT_SPECIFIED);
                    String stepItemDescription = stepItem.optString("description", JSON_CODE_NOT_SPECIFIED);
                    String stepItemVideoURL = stepItem.optString("videoURL", JSON_CODE_NOT_SPECIFIED);
                    String stepItemThumbnailURL = stepItem.optString("thumbnailURL", JSON_CODE_NOT_SPECIFIED);
                    stepsList.add(new Step(stepItemId, stepItemShortDescription, stepItemDescription, stepItemVideoURL, stepItemThumbnailURL));
                }

                String recipeServings = recipeItem.optString("servings", JSON_CODE_NOT_SPECIFIED);
                String recipeImage = recipeItem.optString("image", JSON_CODE_NOT_SPECIFIED);
                recipes.add(new Recipe(recipeId, recipeName, ingredientsList, stepsList, recipeServings, recipeImage ));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Problem parsing JSON results", e);
        }
        return recipes;
    }
}
