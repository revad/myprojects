package com.example.revad.bakingrecipes;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.RemoteViews;

import java.util.ArrayList;

public class BakingWidget extends AppWidgetProvider {

    private static ArrayList<Ingredient> rIngredients;
    private static String rName;
    private static int rNumber;

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {

        if (TextUtils.isEmpty(rName) | rIngredients ==  null) {
            Intent intentMain = new Intent(context, MainActivity.class);
            intentMain.putExtra("started_by", "widget");
            intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intentMain);
        }

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.baking_widget);
        views.setTextViewText(R.id.widget_title, rName);

        Intent intent = new Intent(context, WidgetListService.class);
        Bundle extras = new Bundle();
        extras.putParcelableArrayList("ingredients_object", rIngredients);
        extras.putString("recipe_name", rName);
        intent.putExtra("bundle", extras);
        views.setRemoteAdapter(R.id.widget_list, intent);

        Intent clickIntent = new Intent(context, MainActivity.class);
        clickIntent.putExtra("open_recipe", rNumber);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.widget_title, pendingIntent);
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    public static void updateBakingWidgetWithData(Context context, AppWidgetManager appWidgetManager, String  name, ArrayList<Ingredient> ingredients, int number) {
        rIngredients = ingredients;
        rName = name;
        rNumber = number;
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

