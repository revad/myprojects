package com.example.revad.booklisting;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

/**
 * Created by revad on 3/14/2018.
 */

public class BooksAdapter extends ArrayAdapter<Book> {

    public BooksAdapter(@NonNull Context context, List<Book> books) {
        super(context, 0, books);
    }

    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        Book currentBook = getItem (position);

        TextView authorView = (TextView) listItemView.findViewById(R.id.authorView);
        for (int i=0; i<currentBook.getAuthor().length; i++) {
            if (i>0) authorView.append("\n");
            authorView.append(currentBook.getAuthor()[i]);
        }
        TextView titleView = (TextView) listItemView.findViewById(R.id.titleView);
        titleView.setText(currentBook.getTitle());

        ViewGroup.LayoutParams params = listItemView.getLayoutParams();
        params.height = 0;
        listItemView.setLayoutParams(params);

        return listItemView;
    }
}
