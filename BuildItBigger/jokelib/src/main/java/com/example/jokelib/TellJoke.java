package com.example.jokelib;

public class TellJoke {

    private String [] jokesArray = {
                "Q. What is the biggest lie in the entire universe? A. I have read and agree to the Terms & Conditions.",
                "What do you mean I'm not a bear? I have all of the koalafications!",
                "I just got fired from my job at the keyboard factory. They told me I was not putting in enough shifts.",
                "I hide photos on my computer of me petting animals at the zoo in a file named Fireworks And Vacuums so my dog will not find them.",
                "Q: Why did the computer show up at work late? A: It had a hard drive.",
                "Q: Why was the cell phone wearing glasses? A: It lost its contacts.",
                "You know you're texting too much when you say LOL in real life, instead of just laughing."
            };

    public String tellAJoke () {
        int r = (int) (Math.random() * 7);
        return jokesArray[r];
    }
}
