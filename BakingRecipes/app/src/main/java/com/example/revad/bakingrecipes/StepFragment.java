package com.example.revad.bakingrecipes;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.squareup.picasso.Picasso;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StepFragment extends Fragment {

    private static final String TAG = MainActivity.class.getSimpleName();
    private Context mContext;
    private Step mStep;
    private int stepNumber;
    private static MediaSessionCompat mMediaSession;
    private PlaybackStateCompat.Builder mStateBuilder;
    private SimpleExoPlayer mExoPlayer;
    private Long playerPosition;
    private Boolean playerState;
    @BindView(R.id.shortDescriptionView) TextView shortDescriptionView;
    @BindView(R.id.descriptionView) TextView descriptionView;
    @BindView(R.id.playerView) SimpleExoPlayerView mPlayerView;
    @BindView(R.id.step_image) ImageView stepImage;
    @BindView(R.id.prev_step_button) FloatingActionButton backButton;
    @BindView(R.id.next_step_button) FloatingActionButton nextButton;

    public StepFragment() {
    }

    public void setStep (Step step, int num) {
        mStep = step;
        stepNumber = num;
    }

    private Callbacks mCallbacks;

    public interface Callbacks {
        void onBackButtonClicked(int stepNumber);
        void onNextButtonClicked(int stepNumber);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = null;
        if (context instanceof Activity) { activity = (Activity) context; }
        mCallbacks = (Callbacks) activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        playerPosition = 0L;
        if (savedInstanceState != null) {
            mStep = savedInstanceState.getParcelable("STEP");
            stepNumber = savedInstanceState.getInt("STEP_NUM");
            playerPosition = savedInstanceState.getLong("PLAYER_POSITION");
            playerState= savedInstanceState.getBoolean("PLAYER_STATE");
        }

        mContext = getContext();
        final View view = inflater.inflate(R.layout.fragment_step, container, false);
        ButterKnife.bind(this, view);

        shortDescriptionView.setText(mStep.getShortDescription());
        descriptionView.setText(mStep.getDescription());
        if ( TextUtils.isEmpty(mStep.getThumbnailURL()) ) stepImage.setVisibility(View.GONE);
            else {
            Picasso.with(mContext)
                    .load(mStep.getThumbnailURL())
                    .into(stepImage);
        }

        if (stepNumber == 0) { backButton.setVisibility(View.INVISIBLE); }
            else backButton.setVisibility(View.VISIBLE);
        if (stepNumber == RecipeFragment.getRecipe().getSteps().size() - 1) { nextButton.setVisibility(View.INVISIBLE); }
            else nextButton.setVisibility(View.VISIBLE);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onBackButtonClicked(stepNumber);
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onNextButtonClicked(stepNumber);
            }
        });

        if ( TextUtils.isEmpty(mStep.getVideoURL()) | TextUtils.equals(mStep.getVideoURL(), "Not specified") ) {
            mPlayerView.setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("STEP", mStep);
        outState.putInt("STEP_NUM", stepNumber);
        outState.putLong("PLAYER_POSITION", playerPosition);
        outState.putBoolean("PLAYER_STATE", playerState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPlayerView.getVisibility() == View.VISIBLE) {
            initializeMediaSession();
            initializePlayer(Uri.parse(mStep.getVideoURL()));
        }
    }

    private void initializeMediaSession() {
        mMediaSession = new MediaSessionCompat(mContext, TAG);
        mMediaSession.setFlags(
                MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                        MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mMediaSession.setMediaButtonReceiver(null);
        mStateBuilder = new PlaybackStateCompat.Builder()
                .setActions(
                        PlaybackStateCompat.ACTION_PLAY |
                                PlaybackStateCompat.ACTION_PAUSE |
                                PlaybackStateCompat.ACTION_PLAY_PAUSE);
        mMediaSession.setPlaybackState(mStateBuilder.build());
        mMediaSession.setActive(true);
    }

    private void initializePlayer(Uri mediaUri) {
        if (mExoPlayer == null) {
            TrackSelector trackSelector = new DefaultTrackSelector();
            LoadControl loadControl = new DefaultLoadControl();
            mExoPlayer = ExoPlayerFactory.newSimpleInstance(mContext, trackSelector, loadControl);
            mPlayerView.setPlayer(mExoPlayer);
            String userAgent = Util.getUserAgent(mContext, "Baking Recipes");
            MediaSource mediaSource = new ExtractorMediaSource(mediaUri, new DefaultDataSourceFactory(
                    mContext, userAgent), new DefaultExtractorsFactory(), null, null);
            mExoPlayer.seekTo(playerPosition);
            mExoPlayer.prepare(mediaSource);
            if (playerState != null) mExoPlayer.setPlayWhenReady(playerState);
                else mExoPlayer.setPlayWhenReady(true);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mExoPlayer != null) {
            playerPosition = mExoPlayer.getCurrentPosition();
            playerState = mExoPlayer.getPlayWhenReady();
            mExoPlayer.stop();
            mExoPlayer.release();
            mExoPlayer = null;
        }
    }
}
